'use strict';

var confusionApp = angular.module('confusionApp', ['ui.router', 'ngResource']);

confusionApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
	
    $stateProvider.state('app', {
            url: '/',
            views: {
                'header': {
                    templateUrl : 'views/header.html'
                },
                'content': {
                    templateUrl : 'views/home.html',
                    controller  : 'IndexController'
                },
                'footer': {
                    templateUrl : 'views/footer.html'
                }
            }
    }).state('app.aboutus', {
        url: 'aboutus',
        views: {
            'content@': {
                templateUrl: 'views/aboutus.html',
                controller  : 'AboutController'
            }
        }
    }).state('app.contactus', {
        url: 'contactus',
        views: {
            'content@': {
                templateUrl : 'views/contactus_backed.html',
                controller  : 'ContactController'
            }
        }
    }).state('app.menu', {
        url: 'menu',
        views: {
            'content@': {
                templateUrl : 'views/menu_backed.html',
                controller  : 'MenuController'
            }
        }
    }).state('app.dishdetails', {
        url: 'menu/:id',
        views: {
            'content@': {
                templateUrl : 'views/dishdetail_backed.html',
                controller  : 'DishDetailController'
            }
        }
    }).state('app.promotion', {
        url: 'promotion/:id',
        views: {
            'content@': {
                templateUrl : 'views/dishdetail_backed.html',
                controller  : 'DishDetailController'
            }
        }
    });    
});
