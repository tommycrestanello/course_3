'use strict';

angular.module('confusionApp').constant("baseURL","http://localhost:3000/")
    //.service('corporateFactory', ['$resource', 'baseURL', function($resource, baseURL ) {
    .factory('corporateFactory', ['$resource', 'baseURL', function($resource, baseURL ) {
	var corpfac = {};
    
	corpfac.getLeaders = function() {
	    return $resource(baseURL+"leadership/:id",null,{});
	};
	
	return corpfac;
    }]);
	
angular.module('confusionApp').factory('feedbackFactory', ['$resource', 'baseURL', function($resource,baseURL) {
			
			var feedBack = {};
			
         	feedBack.getFeedback = function() {
          
					return $resource(baseURL+"feedback/:id");
        	};
        	
			return feedBack;
    	}]);

angular.module('confusionApp').constant("baseURL","http://localhost:3000/")
    .service('menuFactory', ['$resource', 'baseURL', function($resource, baseURL ) {
	
	this.getDishes = function(){
            return $resource(baseURL+"dishes/:id",null,{'update':{method:'PUT'}});
	};
	
	this.getPromotion = function() {
	    return $resource(baseURL+"promotions/:id",null,{});
	};
	
    }]);
